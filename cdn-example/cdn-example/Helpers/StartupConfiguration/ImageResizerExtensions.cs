﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using Imageflow.Server;
using Imageflow.Server.DiskCache;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Imageflow.Fluent;
using System;
using Imageflow.Server.HybridCache;

namespace cdn_example.Helpers.StartupConfiguration
{
    public static class ImageResizerExtensions
    {
        public static void AddImageResizer(this IServiceCollection services, IWebHostEnvironment env)
        {
            services.AddDistributedMemoryCache();
            services.AddMemoryCache();
            //services.AddImageflowDiskCache(new DiskCacheOptions(Path.Combine(env.ContentRootPath, "files/temp/image-cache")));
            services.AddImageflowHybridCache(
             new HybridCacheOptions(Path.Combine("files", "temp", "image-cache"))
             {
                    // How long after a file is created before it can be deleted
                    MinAgeToDelete = TimeSpan.FromSeconds(10),
                    // How much RAM to use for the write queue before switching to synchronous writes
                    QueueSizeLimitInBytes = 100 * 1000 * 1000,
                    // The maximum size of the cache (1GB)
                    CacheSizeLimitInBytes = 1024 * 1024 * 1024,
             });
        }

        public static void UseImageResizer(this IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseImageflow(new ImageflowMiddlewareOptions()
                // Maps / to WebRootPath
                .SetMapWebRoot(true)
              // You can get a license key at https://imageresizing.net/
              .SetMyOpenSourceProjectUrl("https://github.com/imazen/imageflow-dotnet-server")
              // Maps /folder to WebRootPath/folder
              .MapPath("/files", Path.Combine(env.ContentRootPath, "files"))
                // Allow localhost to access the diagnostics page or remotely via /imageflow.debug?password=fuzzy_caterpillar
                //.SetDiagnosticsPageAccess(env.IsDevelopment() ? AccessDiagnosticsFrom.AnyHost : AccessDiagnosticsFrom.LocalHost)
                //.SetDiagnosticsPagePassword("fuzzy_caterpillar")
                // Allow HybridCache or other registered IStreamCache to run
                .SetAllowCaching(true)
                // Cache publicly (including on shared proxies and CDNs) for 30 days
                .SetDefaultCacheControlString("public, max-age=2592000")
                // Allows extensionless images to be served within the given directory(ies)
               // .HandleExtensionlessRequestsUnder("/custom_blobs/", StringComparison.OrdinalIgnoreCase)
                // Force all paths under "/gallery" to be watermarked
              
                .AddCommandDefault("down.filter", "mitchell")
                .AddCommandDefault("f.sharpen", "15")
                .AddCommandDefault("webp.quality", "90")
                .AddCommandDefault("ignore_icc_errors", "true")
                //When set to true, this only allows ?preset=value URLs, returning 403 if you try to use any other commands. 
                .SetUsePresetsExclusively(false)
                .AddPreset(new PresetOptions("large", PresetPriority.DefaultValues)
                    .SetCommand("width", "1024")
                    .SetCommand("height", "1024")
                    .SetCommand("mode", "max"))
                // When set, this only allows urls with a &signature, returning 403 if missing/invalid. 
                // Use Imazen.Common.Helpers.Signatures.SignRequest(string pathAndQuery, string signingKey) to generate
                //.ForPrefix allows you to set less restrictive rules for subfolders. 
                // For example, you may want to allow unmodified requests through with SignatureRequired.ForQuerystringRequests
                // .SetRequestSignatureOptions(
                //     new RequestSignatureOptions(SignatureRequired.ForAllRequests, new []{"test key"})
                //         .ForPrefix("/logos/", StringComparison.Ordinal, 
                //             SignatureRequired.ForQuerystringRequests, new []{"test key"}))
                // It's a good idea to limit image sizes for security. Requests causing these to be exceeded will fail
                // The last argument to FrameSizeLimit() is the maximum number of megapixels
                .SetJobSecurityOptions(new SecurityOptions()
                    .SetMaxDecodeSize(new FrameSizeLimit(8000, 8000, 40))
                    .SetMaxFrameSize(new FrameSizeLimit(8000, 8000, 40))
                    .SetMaxEncodeSize(new FrameSizeLimit(8000, 8000, 20)))
                    // Register a named watermark that floats 10% from the bottom-right corner of the image
                    // With 70% opacity and some sharpness applied. 
                   );


        }

    }
}
