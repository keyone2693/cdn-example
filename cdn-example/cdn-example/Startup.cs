
using System.IO;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using cdn_example.Helpers.StartupConfiguration;
using Serilog;


namespace cdn_example
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private IWebHostEnvironment Env { get; }
        private readonly int? _httpsPort;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
            if (env.IsDevelopment())
            {
                var lunchJsonConf = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile("Properties\\launchSettings.json")
                    .Build();
                _httpsPort = lunchJsonConf.GetValue<int>("iisSettings:iisExpress:sslPort");
            }
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRahDbContext(Configuration);
            services.AddRahInitialize(_httpsPort);
            services.AddSignalR();
            services.AddAutoMapper(typeof(Startup));

            services.AddImageResizer(Env);     


    }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRahExceptionHandle(env);


            app.UseImageResizer(env);

            app.UseRahInitialize(env);

            app.UseSerilogRequestLogging();

            app.UseEndpoints(end =>
            {
                end.MapControllers();
                end.MapControllerRoute(
                     name: "default",
                   pattern: "{controller=home}/{action=index}");
                //end.MapFallbackToController("Index", "FallBack");
                //end.MapHub<ChatHubService>(SiteV1Routes.BaseChatPanel + "/chat");

            });

        }
    }
}
